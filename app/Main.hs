{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE OverloadedStrings         #-}
module Main where

import           Notebook.Databricks

import           Prelude               hiding (FilePath, lines)
import qualified Prelude               (FilePath)

import           Turtle

import           Data.Monoid           (mempty, (<>))

import           Control.Monad         (forM_)
import           Control.Monad.Managed (MonadManaged)

import           Options.Applicative   as Opt

import           Data.Char             (isAscii)

import           Data.Maybe            (isNothing)

import           Data.ByteString.Lazy  (ByteString)
import qualified Data.ByteString.Lazy  as B

import           Data.Text             (Text)
import qualified Data.Text             as T
import qualified Data.Text.Encoding    as T

import           Control.Lens          hiding (view, (.=), (<.>))
import qualified Control.Lens          as Lens (view)

import qualified Data.HashMap.Lazy     as HM

import qualified Data.Vector           as V

import           Data.Aeson            ((.=))
import qualified Data.Aeson            as A
import qualified Data.Yaml             as Y

import           Filesystem.Path      (addExtensions, extensions)

data Run = Run { runInfile :: FilePath, runOutpath :: FilePath }

infileP :: Parser FilePath
infileP = Opt.argument str (metavar "INPUT")

outpathP :: Parser FilePath
outpathP = Opt.argument str (metavar "OUTPUT-DIR")

run :: Parser Run
run = Run <$> infileP <*> outpathP

opts :: ParserInfo Run
opts = info (run <**> helper)
  ( fullDesc
  <> progDesc "Split databricks archive" )

isHeaderLine :: Text -> Bool
isHeaderLine = ("//%" `T.isPrefixOf`)

hasHeader :: DBCommand -> Bool
hasHeader c = any isHeaderLine (c^.dbcCommand.lines)

getHeader :: DBCommand -> Maybe Y.Value
getHeader c = Y.decode (T.encodeUtf8 stripped)
  where header = filter isHeaderLine (c^.dbcCommand.lines)
        stripped = T.unlines (map (T.strip . T.drop 3) header)

getTags :: DBCommand -> Maybe [Y.Value]
getTags c = getTags' =<< getHeader c
  where getTags' (Y.Object o) = oneOrMany =<< HM.lookup "tag" o
        getTags' _            = Nothing
        oneOrMany (Y.Array ts) = Just (V.toList ts)
        oneOrMany t            = Just [t]

stripHeader :: DBCommand -> DBCommand
stripHeader = over dbcCommand stripHeader'
  where stripHeader' = over lines (dropWhile isHeaderLine)

stripHeaders :: [DBCommand] -> [DBCommand]
stripHeaders = map stripHeader

studentCommands :: [DBCommand] -> [DBCommand]
studentCommands = onlyTags True ["exercise", "test"]

solutionCommands :: [DBCommand] -> [DBCommand]
solutionCommands = onlyTags True ["answer", "test"]

privateTestCommands :: [DBCommand] -> [DBCommand]
privateTestCommands = onlyTags False ["private_test"]

onlyTags :: Bool -> [Text] -> [DBCommand] -> [DBCommand]
onlyTags includeUntagged validTags = filter isValid
  where isValid c =
          case getTags c of
            Nothing ->
              if hasHeader c
              then error ("Invalid Header: " ++ T.unpack (c^.dbcCommand))
              else includeUntagged
            Just tags -> any isValidTag tags
        isValidTag (Y.String t) = T.toLower t `elem` validTags
        isValidTag _            = False

makeSafeName :: Text -> FilePath
makeSafeName title = fromString (map makeSafeChar (T.unpack title))
  where makeSafeChar c | isUnsafe c = '_'
                       | otherwise = c
        isUnsafe c = not (isAscii c) || (c `elem` unsafeChars)
        unsafeChars :: String
        unsafeChars = "/\\ \"'~"

data PrivateTest = PrivateTest { ptTitle   :: Text
                               , ptSuccess :: Text
                               , ptFailure :: Text
                               , ptCode    :: Text }
  deriving (Eq, Show)

instance A.ToJSON PrivateTest where
  toJSON (PrivateTest title success failure code) =
    A.object [ "title" .= title
             , "success" .= success
             , "failure" .= failure
             , "text" .= code ]

  toEncoding (PrivateTest title success failure code) =
    A.pairs $ ("title" .= title)
           <> ("success" .= success)
           <> ("failure" .= failure)
           <> ("text" .= code)

privateTestHeader :: DBCommand -> Maybe PrivateTest
privateTestHeader c = do
  let asObject (Y.Object o) = Just o
      asObject _            = Nothing
      asString (Y.String s) = Just s
      asString _            = Nothing

  header <- asObject =<< getHeader c
  title <- asString =<< HM.lookup "title" header
  let success = maybe "Passed" id (asString =<< HM.lookup "success" header)
      failure = maybe "Failed" id (asString =<< HM.lookup "failure" header)
      code = (stripHeader c)^.dbcCommand

  return (PrivateTest title success failure code)

privateTests :: [DBCommand] -> [PrivateTest]
privateTests cs = maybe failurem id (sequence tests)
  where tests = map privateTestHeader cs
        failures = map (^.dbcCommand) $ filter (isNothing . privateTestHeader) cs
        failurem = error ("Failed to parse headers: " ++ concatMap (++"\n\n") (map (T.unpack) failures))

expandShorthands :: [DBCommand] -> [DBCommand]
expandShorthands = map expandShorthand

expandShorthand :: DBCommand -> DBCommand
expandShorthand = over (dbcCommand . lines) (map expandShorthand')
  where expandShorthand' line | "//#" `T.isPrefixOf` line =
                                  "//% tag: " <> (T.drop 3 line)
                              | otherwise = line

lines :: Iso' Text [Text]
lines = iso T.lines T.unlines

gfp :: FilePath -> Prelude.FilePath
gfp = T.unpack . format fp

addSuffix :: Text -> FilePath -> FilePath
addSuffix suffix p = (dirname p) </> (basename p `addExtensions` (suffix : extensions p))

makeTransformed
  :: FilePath -> FilePath -> FilePath
  -> ([DBCommand] -> [DBCommand])
  -> Shell ()
makeTransformed srcfolder outpath name coms = sh $ do
  resultFolder <- using (mktempdir "/tmp" "dbcparse-result")
  sh $ do
    srcfile <- find isSourceFile srcfolder
    let Just relfile = stripPrefix (srcfolder </> "") srcfile
    srcontent <- liftIO (B.readFile (gfp srcfile))
    parsed <- either (die . T.pack) return (fromByteString srcontent)
    let transformed = over dbnCommands coms parsed
        newname = addSuffix (format fp name) relfile
        final = over dbnName (<> "." <> (format fp name)) transformed
    liftIO $ B.writeFile (gfp (resultFolder </> newname)) (toByteString final)
  _ <- procStrict "jar" [ "-Mcf", format fp (outpath </> name <.> "dbc")
                        , "-C", format fp resultFolder, "."] mempty
  return ()

makeJsons
  :: (Y.ToJSON a) =>
     FilePath -> FilePath -> (a -> FilePath)
  -> ([DBCommand] -> [a])
  -> Shell ()
makeJsons srcfolder outdir getName coms = do
  resultFolder <- using (mktempdir "/tmp" "dbcparse-result")
  sh $ do
    srcfile <- find isSourceFile srcfolder
    let Just relfile = stripPrefix (srcfolder </> "") srcfile
    srcontent <- liftIO (B.readFile (gfp srcfile))
    parsed <- either (die . T.pack) return (fromByteString srcontent)
    let extracted = coms (parsed^.dbnCommands)
    forM_ extracted $ \ex -> do
      liftIO $ B.writeFile (gfp (outdir </> getName ex <.> "json")) (A.encode ex)

isSourceFile :: Pattern Text
isSourceFile = choice [suffix s | s <- [".scala", ".py", ".R", ".sql"]]

main :: IO ()
main = do
  (Run infile outpath) <- execParser opts
  sh $ do
    srcfolder <- using (mktempdir "/tmp" "dbcparse-source")
    _ <- procStrict "unzip" [format fp infile, "-d", format fp srcfolder] mempty
    --
    mktree outpath
    makeTransformed srcfolder outpath "exercises"
      (stripHeaders . studentCommands . expandShorthands)
    makeTransformed srcfolder outpath "solutions"
      (stripHeaders . solutionCommands . expandShorthands)
    mktree (outpath </> "tests")
    makeJsons srcfolder (outpath </> "tests")
      (makeSafeName . ptTitle)
      (privateTests . privateTestCommands . expandShorthands)
